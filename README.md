# Demo Project CD Deploy to EKS cluster from Jenkins Pipeline 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Install kubectl and aws-iam-authenticator on a Jenkins server

* Create kubeconfig file to connect to EKS cluster and add it on Jenkins server

* Add AWS credentials on Jenkins for AWS account authentication

* Extend and adjust Jenkinsfile of CI/CD pipeline to configure connection to EKS cluster

## Technologies Used 

* Kubernetes

* Jenkins 

* AWS EKS 

* Docker 

* Linux 

## Steps 

Step 1: Get inside Jenkins running container through interactive terminal as root user 

    docker exec -u 0 -it dfb5b5a791bd /bin/bash

[Command to get inside jenkins container](/images/01_get_inside_jenkins_running_container_through_interactive_terminal_as_root_user.png)

Step 2: Download kubectl on jenkins server 

    curl -LO "https://d1.k8s.io/$(curl -L -s https://d1.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

[Downloading Kubectl on jenkins server](/images/02_download_kubectl_on_jenkins_server.png)

Step 3: Add execution permission to kubectl 

    chmod +x ./kubectl 

[Adding execution permission to kubectl](/images/03_add_execution_permission_to_kubectl.png)


Step 4: Move kubectl to binary directory 

    mv ./kubectl /usr/local/bin/kubectl 

[Moving kubectl to binary directory](/images/04_move_kubectl_to_binary_directory.png)

[Kubectl available](/images/05_kubectl_available.png)

Step 5: Download aws iam authenticator on Jenkins server 

    curl -Lo aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v0.5.9/aws-iam-authenticator_0.5.9_linux_amd64

[Downloading iam authenticator](/images/06_download_aws_iam_authenticator_to_jenkins_container.png)

Step 6: give aws iam authenticator execution permission 

    chmod +x aws-iam-authenticator 

[Execution permission on iam authenticator](/images/07_gave_aws_iam_auth_execution_permission.png)

Step 7: Move aws iam authenticator to binary directory 

    mv ./aws-iam-authenticator /usr/local/bin

[Moving aws iam authenticator to binary directory](/images/08_move_aws_iam_auth_to_binary_directory.png)

[iam authenticator available](/images/09_aws_iam_authenticator_available.png)

Step 8: Create config file on server running jenkins container, this is because jenkins container does not have any editor like vim or nano 

    vim config 

[Create config file](/images/10_creating_config_file_on_the_server_running_jenkins_this_is_because_jenkins_contianer_does_not_have_any_editors.png)

Step 9: Go into Jenkins container 

    docker exec -it dfb5b5a791bd /bin/bash

[Going in Jenkins container](/images/11_go_into_jenkins_container.png)

Step 10: Create kube directory in jenkins home directory 

    mkdir ~/.kube 

[Kube directory](/images/12_create_kube_directory_in_jenkins_home_directory.png)

Step 11: exit Jenkins container and go back to the server that is running jenkins container and copy config file to .kube directory in home directory of jenkins container 

    exit 
    docker cp config dfb5b5a791bd:/var/jenkins_home/.kube 

[Copying config file into Jenkins container](/images/13_exit_jenkins_container_going_back_to_the_server_which_is_running_jenkins_and_copy_config_file_to_jenkins_container.png)

Step 12: Confirm if config file has been copied to .kube directory 

    docker exec -it dfb5b5a791bd /bin/bash
    ls ~/.kube 

[config cp success](/images/14_confirm_if_it_was_successful_copied_into_jenkins_container_.png)

Step 13: Create credential for multibranch pipeline job scope for aws access key 

[Jenkins aws access key cred](/images/15_creating_credentials_on_jenkins_multibranch_pipeline_for_access_key_id_of_aws_user_that_can_be_founs_in_secret_aws_directory_in_home_directory.png)

Step 14: Create credential for multibranch pipeline job scope for aws secret access key 

[Jenkins aws secret access key cred](/images/16_create_credentials_on_jenkins_multibranch_pipeline_for_secret_access_key_id.png)

Step 15: Confirm created credentials 

[Created credentials](/images/17_created_credentials.png)

Step 16: Create Jenkinsfile that deplys to eks, this will need you will need to set environmental variables for aws credentials to allow you deployn on eks 

[Jenkinsfile deployment stage](/images/18_create_jenkinsfile_that_deploys_to_eks_you_will_need_to_set_env_variables_for_aws_credentials_to_allow_you_deploy_on_eks.png)

Step 17: Build the Pipeline 

[Built Pipeline](/images/19_build_pipeline.png)

[Console Output](/images/20_pipeline_console_output.png)

Step 18: Confirm if application was successfully deployed in eks cluster 

    kubectl get pods 

[pods](images/21_active_pod.png)


## Installation

    curl -LO "https://d1.k8s.io/$(curl -L -s https://d1.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

## Usage 

    kubectl create deployment nginx-deployment --image=nginx

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/cd-deploy-to-eks-cluster-from-jenkins-pipeline.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/cd-deploy-to-eks-cluster-from-jenkins-pipeline

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.